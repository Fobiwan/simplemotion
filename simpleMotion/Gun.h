#ifndef GUN_H
#define GUN_H


#include "GameObj.h"
#include "Ball.h"

//cannon spins around and can fire one cannon ball
class Gun : public GameObj
{
public:
	Gun()
		:GameObj(), mWaitSecs(0)
	{}
	void Update();
	void RenderBall(sf::RenderWindow& window);
private:
	float mWaitSecs;	//delay after firing beforeo you can move/
	Ball mBall;		//ball
	
};


#endif // !GUN_H