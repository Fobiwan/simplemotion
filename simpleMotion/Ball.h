#ifndef BALL_H
#define BALL_H

#include "GameObj.h"
#include "Wall.h"

class Ball : public GameObj {
public:
	Ball();
	void update(float dT);
	void CheckForCollision(Vec2f& newPos);
	void setBall(Vec2f position, Vec2f& direction, Vec2f& velocity);
	Vec2f getDir();
private:
	Vec2f dir;
	sf::Texture mTex;
	float mWaitSecs;
	float ballWidth = 40.f;
	float ballHeight = 40.f;
};
#endif // !BALL_H
