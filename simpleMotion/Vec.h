#ifndef VEC_H
#define VEC_H

#include "SFML/Graphics.hpp"

//dimensions in 2D that are whole numbers
struct Vec2i
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
class Vec2f
{
public:
	Vec2f();
	Vec2f(float, float);
	Vec2f(Vec2f&, float);
	explicit Vec2f(sf::Vector2f&);
	bool operator!=(const Vec2f& other);
	Vec2f operator*(float scalar);
	Vec2f& operator*=(float scalar);
	Vec2f operator+(const Vec2f& other);
	Vec2f& operator+=(const Vec2f& other);
	Vec2f operator-();
	Vec2f operator-(const Vec2f& other);
	Vec2f& operator-=(const Vec2f& other);
	Vec2f operator/(float scalar);
	Vec2f& operator/=(float scalar);
	bool operator==(const Vec2f& other);

	Vec2f& normalise();
	void rotate(float angle);

	float x, y;

private:
};


#endif // !VEC_H

