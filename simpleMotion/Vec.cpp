#include "Vec.h"

#include "SFML/Graphics.hpp"

const float PI = 3.14159265358979323846f;
inline float Deg2Rad(float deg) {
	return deg * (PI / 180.f);
}

Vec2f::Vec2f()
{
	x = 0;
	y = 0;
}

Vec2f::Vec2f(float f1, float f2) {
	x = f1;
	y = f2;

}

Vec2f::Vec2f(Vec2f& dir, float speed) {
	x = dir.x * speed;
	y = dir.y * speed;
}

Vec2f::Vec2f(sf::Vector2f& vec) {
	x = vec.x;
	y = vec.y;
}


bool Vec2f::operator!=(const Vec2f& other) {
	return (other.x != this->x && other.y != this->y);
}

Vec2f Vec2f::operator *(float scalar) {
	return Vec2f(this->x*scalar, this->y*scalar);

}

Vec2f& Vec2f::operator *=(float scalar) {
	x *= scalar;
	y *= scalar;
	return *this;
}

Vec2f Vec2f::operator +(const Vec2f& other) {
	return Vec2f(this->x + other.x, this->y + other.y);
}

Vec2f& Vec2f::operator +=(const Vec2f& other) {
	x += other.x;
	y += other.y;
	return *this;
}

Vec2f Vec2f::operator-() {
	return Vec2f(-x, -y);
}

Vec2f Vec2f::operator- (const Vec2f& other) {
	return Vec2f(x - other.x, y - other.y);
}

Vec2f& Vec2f::operator -=(const Vec2f& other) {
	x -= other.x;
	y -= other.y;
	return *this;
}

Vec2f Vec2f::operator/(float scalar) {
	return Vec2f(x / scalar, y / scalar);
}

Vec2f& Vec2f::operator/=(float scalar) {
	x /= scalar;
	y /= scalar;
	return *this;
}

bool Vec2f::operator ==(const Vec2f& other) {
	return (other.x == this->x && other.y == this->y);
}

Vec2f& Vec2f::normalise() {
	float sqLen = (x*x) + (y*y);
	float len = sqrt(sqLen);
	x /= len;
	y /= len;
	return *this;
}

void Vec2f::rotate(float angle) {
	Vec2f old(x, y);
	float oldLen = sqrtf((x*x) + (y*y));
	x = cosf(angle*(PI / 180));
	y = sinf(angle*(PI / 180));
}
