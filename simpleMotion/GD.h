#ifndef GD_H
#define GD_H

#include "SFML/Graphics.hpp"


struct GD
{
	sf::Font font;				//a shared font to use everywhere
	sf::RenderWindow *pWindow;	//can't render anything without this
	std::string playerName;			//the player's name is needed by different objects
};
#endif // !GD_H
