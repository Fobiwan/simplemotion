#include "Ball.h"
#include "Vec.h"
#include "GDC.h"
#include <iostream>


Ball::Ball()
	:GameObj(), mWaitSecs(0)
{

	if (!mTex.loadFromFile("data/ball.png"))
		std::cout << "ball not loaded";
	SetTexture(mTex);
	SetOrigin(Vec2f(ballWidth / 2, ballHeight / 2));

}

Vec2f Ball::getDir() {
	return dir;
}


void Ball::setBall(Vec2f position, Vec2f& direction, Vec2f& velocity) {
	SetPos(position);
	dir = direction;
	SetVel(velocity);
}


void Ball::CheckForCollision(Vec2f& newPos) {
	//Size of wall was weird and not easy to figure out with given constants, used random values that fit the screen so it looked good
	if (newPos.x > (GDC::SCREEN_RES.x - GDC::SCREEN_RES.x / 7 + ballWidth) || newPos.x < GDC::SCREEN_RES.x / 13 + ballWidth) {
		Vec2f velTmp = GetVel();
		velTmp.x *= -1;
		SetVel(velTmp);
	}
	if (newPos.y > (GDC::SCREEN_RES.y - GDC::SCREEN_RES.y / 6 + ballHeight) || newPos.y < (GDC::SCREEN_RES.y / 13 + ballHeight)) {
		Vec2f velTmp = GetVel();
		velTmp.y *= -1;
		SetVel(velTmp);
	}
}


void Ball::update(float dT) {
	Vec2f posTmp = GetPos();
	Vec2f velTmp = GetVel();
	Vec2f newPos = posTmp + velTmp * 1 / dT;
	CheckForCollision(newPos);
	SetPos(newPos);
}