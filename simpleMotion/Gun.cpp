#include "Gun.h"
#include "Application.h"
#include "Ball.h"
#include "Vec.h"

#include <iostream>

bool ballSet = false;

const float PI = 3.14159265358979323846f;
float Deg2Rad(float deg) {
	return deg * (PI / 180.f);
}

void Gun::RenderBall(sf::RenderWindow& window) {
	mBall.Render(window);
}

void Gun::Update()
{

	SetPos(Vec2f{ GDC::SCREEN_RES.x / 2.f, GDC::SCREEN_RES.y / 2.f });

	if (ballSet)
		mBall.update(60);


	if (mWaitSecs <= 0)
	{
		//here we don't want windowsOS keys, with initial delays and repeat rate delays and all that rubbish
		//so just access the keyboard directly for instant response
		float inc = 0;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			inc = -GDC::PLAY_SPIN_SPD;
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			inc = GDC::PLAY_SPIN_SPD;
		//a bit of physics degress per seconds X seconds for smooth predictable rate of rotation
		SetDegrees(GetDegrees() + inc * Application::GetElapsedSecs());
	}
	else
		mWaitSecs -= Application::GetElapsedSecs();

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		//fire the ball here
		Vec2f dir(-1, 0);
		dir.rotate(this->GetDegrees() + GDC::CANNON_ROT_OFFSET);
		Vec2f vel(dir, GDC::BALL_SPEED / 60);
		mBall.setBall(this->GetPos() + (dir * GDC::GUN_LENGTH), dir, vel);
		ballSet = true;

		mWaitSecs = GDC::FIRE_DELAY;
	}
}